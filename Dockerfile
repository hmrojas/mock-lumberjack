# ==================================================
# ALSTOM NAM Innovation
# Data Science and AI
# Data API
# --------------------------------------------------
# Dockerfile
# --------------------------------------------------
# - Dockerfile
# --------------------------------------------------
# Copyright (c) 2021 - Alstom USA.
# ==================================================

FROM python:3.7-slim

COPY ./requirements.txt /tmp/

RUN pip3 install -r /tmp/requirements.txt

EXPOSE 8080

COPY app/ /app

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "8080"]
