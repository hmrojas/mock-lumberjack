'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Crossing API
     - timeseries.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
from influxdb import DataFrameClient
from influxdb import InfluxDBClient

# common services
from ..services.common_services import ServiceError


# influxdb connection config
INFLUX_CONNECTION = dict(host='influxdb',
                         port=8086,
                         username='root',
                         password='root',
                         timeout=10)


# influx clients
df_client = DataFrameClient(**INFLUX_CONNECTION)
db_client = InfluxDBClient(**INFLUX_CONNECTION)


def query_influx(
        query: str,
        params: dict = None,
        bind_params: dict = None):
    try:
        response = db_client.query(query, params, bind_params)
        return response
    except Exception as err:  # pylint: disable=broad-except
        raise ServiceError(str(err)) from err


def query_influx_df(
        query: str,
        params: dict = None,
        bind_params: dict = None):
    try:
        response = df_client.query(query, params, bind_params)
        return response
    except Exception as err:  # pylint: disable=broad-except
        raise ServiceError(str(err)) from err


def is_present(database: str) -> bool:
    try:
        response = df_client.query('show databases')
        for item in list(response.get_points()):
            if item['name'] == database:
                return True
        return False
    except Exception as err:  # pylint: disable=broad-except
        raise ServiceError(str(err)) from err


def create_database(
        database: str,
        retention_policy_names: list):
    try:
        if not is_present(database):
            df_client.query(f'create database "{database}"')
            for policy in retention_policy_names:
                df_client.query(
                    f'create retention policy "{policy}_ret_pol" on "{database}" duration '
                    '180d replication 1 shard duration 1h'
                )
        return True
    except Exception as err:  # pylint: disable=broad-except
        raise ServiceError(f'could not create {database}: {str(err)}') from err


def to_influx(
        data,
        database: str,
        measurement: str,
        tag_columns: list,
        retention_policy_names: list):

    try:
        create_database(database, retention_policy_names)
        df_client.write_points(
            data,
            database=database,
            tag_columns=tag_columns,
            retention_policy=f'{measurement}_ret_pol',
            measurement=measurement,
            protocol='line')
        return f'data persisted in {database}'
    except Exception as err:  # pylint: disable=broad-except
        raise ServiceError(f'could not persist {database}: {str(err)}') from err


def delete(database: str):
    try:
        if not is_present(database):
            raise ServiceError(f'{database} not found')
        df_client.drop_database(database)
        return f'database {database} deleted'
    except Exception as err:  # pylint: disable=broad-except
        raise ServiceError(f'could not delete {database}: {str(err)}') from err
