'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
     Version: 0.0.x
    --------------------------------------------------
     Insights - Data Lake Interface
     - insights.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2020 - Alstom USA.
    ==================================================
'''

# Dependencies
from typing import Optional
import datetime as dt

from pyArango import connection as ar
from fastapi import Request

# Import Service Error
from ..services.common_services import DuplicateError, ServiceError


# ============================
#  Insight Data with ArangoDB
# ============================

# arangodb connection config
ARANGO_CONNECTION = dict(username='root',
                         password='82c7Wbuf.74s8',
                         arango_url='http://arangodb:8529')


# Connection method
def connect_to_arangodb(username: str, password: str, arango_url: str):
    try:
        return ar.Connection(username=username,
                             password=password,
                             arangoURL=arango_url)

    except Exception as e:  # pylint: disable=broad-except
        print(f'An error occurred while trying establish a database connection: {str(e)}')
        return f'Could not establish connection to the database. Exception: {str(e)}'


# Initialize standard connection
insights = connect_to_arangodb(**ARANGO_CONNECTION)


# ===============================
#  Database Manipulation Methods
# ===============================

# Activates a specific database or creates it if non-existent.
def activate_database(database_name: str) -> ar.Database:
    return (insights.createDatabase(name=database_name)
            if not insights.hasDatabase(database_name)
            else insights[database_name])


# Activates a specific data collection or creates it if non-existent.
def activate_collection(collection_name: str, database: ar.Database):
    return (database.createCollection(name=collection_name)
            if not database.hasCollection(collection_name)
            else database[collection_name])


# ==============================
#  Database Information Methods
# ==============================

# Returns non-system collections in a given database.
def list_collections_from(database: ar.Database) -> list:
    return [collection
            for collection in database.collections.keys()
            if not collection.startswith('_')]


# ==========================
#  Data Persistence Methods
# ==========================

# Formats a string key for output payloads.
def standardize_key(key: str,  # pylint: disable=R0913
                    defined_key: Optional[str] = None,
                    add_prefix: Optional[str] = None,
                    add_suffix: Optional[str] = None,
                    make_singular: Optional[bool] = False,
                    make_plural: Optional[bool] = False) -> str:

    # If a specific key was defined, enforce it.
    if defined_key is not None:
        return defined_key

    # Make lowercase and replace hyphens with underscores.
    key = key.lower().replace('-', '_')

    # Make singular/plural.
    if make_singular and key.endswith('s'):
        key = key[:-1]
    if make_plural:
        key += 's'

    # Manipulate key with prefixes and/or suffixes.
    if add_prefix is not None:
        key = add_prefix + key
    if add_suffix is not None:
        key += add_suffix

    # Returns formatted key.
    return key


def persist_data(*,
                 data: dict,
                 metadata: Optional[dict] = None,
                 collection,
                 request: Request,
                 data_id: Optional[str] = None,
                 include_nulls: Optional[bool] = False) -> str:
    try:
        # Create a new document in the given collection.
        document = collection.createDocument()

        # Add a key if it was given.
        if data_id:
            # Check if key was featured in the data payload.
            if data_id in data.keys():
                key = data[data_id]
            else:
                key = data_id

            try:
                collection[key]  # pylint: disable=W0104
                raise DuplicateError('Could not persist data. Identifier already exists.')
            except Exception:  # pylint: disable=broad-except
                document['_key'] = key

        # Add meta-data
        document['meta-data'] = dict(
                            {'time-uploaded': dt.datetime.now().isoformat(),
                             'source-ip': request.client.host},
                            **(metadata if metadata else {}))

        # Remove reserved keys from the data, if any; and empty values
        # if not otherwise requested.
        reserved_keys = ['_key', '_rev', '_id']
        document['data'] = {key: value for key, value in data.items()
                            if key not in reserved_keys
                            and (include_nulls or value is not None)}

        # Save document to database.
        document.save()
        result = 'Data persisted to Data Lake.'

    except Exception as e:  # pylint: disable=broad-except
        raise ServiceError(f'Could not persist data. Exception: {str(e)}') from e

    return result


# customized data persistance for asset objects
def persist_asset_data(*,
                       data: dict,
                       collection,
                       device_id: Optional[str] = None,
                       include_nulls: Optional[bool] = False) -> str:

    # default top level dict key
    top_key = 'data'

    # remove top level dict key from data if any
    # if one is found, replace default top level key with it
    if len(list(data.keys())) == 1:
        top_key = list(data.keys())[0]
        data = list(data.values())[0]

    try:
        # Create a new document in the given collection.
        document = collection.createDocument()

        # Add a key if it was given.
        if device_id:
            # Check if key was featured in the data payload.
            if device_id in data.keys():
                key = data[device_id]
            else:
                key = device_id

            try:
                collection[key]  # pylint: disable=W0104
                raise DuplicateError('Could not persist data. Identifier already exists.')
            except Exception:  # pylint: disable=broad-except
                document['_key'] = key

        # Remove reserved keys from the data, if any; and empty values
        # if not otherwise requested.
        reserved_keys = ['_key', '_rev', '_id']
        document[top_key] = {key: value for key, value in data.items()
                             if key not in reserved_keys
                             and (include_nulls or value is not None)}

        # Save document to database.
        document.save()
        result = 'Data persisted to Data Lake.'

    except Exception as e:  # pylint: disable=broad-except
        raise ServiceError(f'Could not persist data. Exception: {str(e)}') from e

    return result


# ========================
#  Data Retrieval Methods
# ========================

# Get documents in a given database and collection.
def get_documents_in(collection_name: str,
                     database_name: str,
                     limit: Optional[int] = 10, skip: Optional[int] = 0,
                     **kwargs) -> tuple:
    try:
        # Collect documents from database and format response payload
        collection = activate_collection(collection_name=collection_name,
                                         database=activate_database(database_name))
        document_query = collection.fetchAll(limit=limit, skip=skip)
        reserved_keys = ['_rev', '_id']
        result = [{(key if not key == '_key' else '_uuid'): document.getStore()[key]   # noqa E501
                   for key in document.getStore().keys()
                   if key not in reserved_keys}
                  for document in document_query]

    except Exception as e:  # pylint: disable=broad-except
        raise ServiceError(f'Error fetching data. Exception: {str(e)}') from e

    # Collect extra function arguments.
    defined_key = (None
                   if 'defined_key' not in kwargs.keys()
                   else kwargs.get('defined_key'))
    add_prefix = (None
                  if 'add_prefix' not in kwargs.keys()
                  else kwargs.get('add_prefix'))
    add_suffix = (None
                  if 'add_suffix' not in kwargs.keys()
                  else kwargs.get('add_suffix'))

    # Returns data with formatted key.
    return {standardize_key(collection_name,
                            defined_key=defined_key,
                            add_prefix=add_prefix,
                            add_suffix=add_suffix,
                            make_singular=False): ([] if not result else result)}


# Count the number of documents in a given database and collection.
def count_documents_in(collection_name: str,
                       database_name: str,
                       **kwargs) -> dict:
    try:
        # Activates specific database and collection, and counts documents.
        collection = activate_collection(collection_name,
                                         activate_database(database_name))
        result = collection.count()
    except Exception as e:  # pylint: disable=broad-except
        raise ServiceError(f'Error counting records. Exception: {str(e)}') from e

    # Collect extra function arguments.
    defined_key = (None
                   if 'defined_key' not in kwargs.keys()
                   else kwargs.get('defined_key'))
    add_prefix = (None
                  if 'add_prefix' not in kwargs.keys()
                  else kwargs.get('add_prefix'))

    # Returns dictionary with formatted key.
    return {standardize_key(collection_name,
                            defined_key=defined_key,
                            add_prefix=add_prefix,
                            add_suffix='_count',
                            make_singular=True): ([] if not result else result)}

# ======================
#  Data Removal Methods
# ======================


# General data removal.
def remove_data(doc_key, collection_name: str, database_name: str):
    try:
        # Collect documents from database and format response payload
        collection = activate_collection(collection_name=collection_name,
                                         database=activate_database(database_name))
        document = collection[doc_key]
        document.delete()
        return 'Data successfully removed.'

    except Exception as e:  # pylint: disable=broad-except
        raise ServiceError(f'Error removing data. Exception: {str(e)}') from e
