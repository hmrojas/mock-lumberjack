'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
     Version: 0.0.x
    --------------------------------------------------
     Crossing API
     - train_data.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import re
import json
from base64 import b64encode

import pandas as pd


# common services
from ..services.common_services import ServiceError


def parse_records(lines: list):
    # remove unwanted lines
    lines = [x for x in lines if re.match(r'^\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', x)]
    lines = [x.split() for x in lines if len(x.split()) == 15]
    # data to dataframe
    data = pd.DataFrame(lines)
    data.columns = ['date', 'time', 'isl', 'mdr', 'aux', 'cwe', 'ap',
                    'rxTrack1', 'phTrack1', 'rxTrack2', 'phTrack2',
                    'rxTrack3', 'phTrack3', 'rxTrack4', 'phTrack4']
    # store each raw record as json
    data['json'] = data.to_dict(orient='records')
    data['json'] = [json.dumps(x) for x in data['json']]
    data['json'] = [b64encode(x.encode('utf-8')).decode('ascii') for x in data['json']]
    # date to datetime
    data['time'] = pd.to_datetime(data['date'] + 'T' + data['time'], utc=True)
    # parse bits
    for bit in range(12):
        if bit < 4:
            col = f'isl{bit + 1}'
            data[col] = pd.to_numeric(data['isl'].str[bit], errors='coerce')
        for sig in ['mdr', 'aux', 'cwe', 'ap']:
            col = f'{sig}{bit + 1}'
            data[col] = pd.to_numeric(data[sig].str[bit], errors='coerce')
    # parse rx and phase
    for col in [x for x in data.columns if 'Track' in x]:
        data[col] = pd.to_numeric(data[col], errors='coerce')
    # prepare for storage in influx
    data.dropna(axis=1, how='all', inplace=True)
    drop_cols = ['date', 'isl', 'mdr', 'aux', 'cwe', 'ap']
    data.drop(drop_cols, axis=1, inplace=True)
    data = data[['time'] + [x for x in list(data.columns) if x != 'time']]
    # convert to nullable boolean
    cols = [x for x in list(data.columns) if re.search(r'mdr|aux|cwe|ap|isl', x)]
    for col in cols:
        data[col] = data[col].astype('boolean')
        data[col] = data[col].astype('boolean')
    # add dummy field
    data['dummy_field'] = '0'
    return data


def parse_recording(lines: list):
    lines = [x for x in lines if re.search(r'Recording Start$', x)]
    lines = [x.replace(' Recording', ',Recording') for x in lines]
    lines = [x.split(',') for x in lines]
    # data for influx
    if lines:
        recording = pd.DataFrame(lines)
        recording.columns = ['time', 'recording']
        recording['time'] = pd.to_datetime(recording['time'], utc=True)
        return recording
    raise ServiceError('no train moves found')


def parse_train_data(lines: list):
    ''' parse crossing train data logs '''
    try:
        data = parse_records(lines)
        recording = parse_recording(lines)
        # merge data records with Recording Start records
        data = data.append(recording, ignore_index=True)
        data.sort_values(['time', 'recording'], ascending=True, inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['count'] = data.groupby('time').cumcount().astype('int').astype('str')
        data.set_index('time', drop=True, inplace=True)
        if not data.empty:
            return data
        raise ServiceError('no train moves found')
    except (ServiceError, Exception) as err:
        raise ServiceError('no train moves found') from err


def parse_system_event(lines: list):
    try:
        data = parse_records(lines)
        data.sort_values('time', ascending=True, inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['count'] = data.groupby('time').cumcount().astype('str')
        data.set_index('time', drop=True, inplace=True)
        if not data.empty:
            return data
        raise ServiceError('no train moves found')
    except (ServiceError, Exception) as err:
        raise ServiceError('no records found') from err
