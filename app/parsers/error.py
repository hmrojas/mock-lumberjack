'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Crossing API
     - error.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import re
from base64 import b64encode

import pandas as pd

# common services
from ..services.common_services import ServiceError


def parse_errors(lines: list):
    """read error logs into a dataframe

    Args:
        lines (list): A list of log lines

    Returns:
        dataframe: dataframe with tidy data
    """
    # remove unwanted lines
    lines = [re.sub(r'(?<=\d{2}:\d{2}:\d{2})\s', ':::', x) for x in lines
             if re.match(r'^\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', x)]
    lines = [x.split(':::') for x in lines]

    # data to dataframe
    data = pd.DataFrame(lines)
    data.columns = ['time', 'data']

    # date to datetime
    data['time'] = pd.to_datetime(data['time'], utc=True)

    # encode data column to base64 and store it as ascii
    data['data'] = [b64encode(x.encode('utf-8')).decode('ascii') for x in data['data']]

    # prepare for storage in influx
    data.dropna(axis=1, how='all', inplace=True)

    # add dummy field
    data['dummy_field'] = '0'
    return data


def parse_error_logs(lines: list):
    ''' parse ring status logs '''
    try:
        data = parse_errors(lines)
        data.sort_values('time', ascending=True, inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['count'] = data.groupby('time').cumcount().astype('int').astype('str')
        data.set_index('time', drop=True, inplace=True)
        if not data.empty:
            return data
        raise ServiceError('no records found')
    except (ServiceError, Exception) as err:
        raise ServiceError('no records found') from err
