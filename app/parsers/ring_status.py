'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
     Version: 0.0.x
    --------------------------------------------------
     Crossing API
     - ring_status.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import re
import json
from base64 import b64encode

import pandas as pd

# common services
from ..services.common_services import ServiceError


def parse_records_to_bits(lines: list):
    # remove unwanted lines
    lines = [re.sub(r'\s(?=\d{2}:\d{2}:\d{2})', 'T', x) for x in lines
             if re.match(r'^\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', x)]
    lines = [x.split() for x in lines if len(x.split()) == 12]
    # data to dataframe
    data = pd.DataFrame(lines)
    data.columns = ['time', 'mdr', 'track',
                    'C72_65', 'C64_57', 'C56_49', 'C48_41', 'C40_33',
                    'C32_25', 'C24_17', 'C16_09', 'C08_01']
    # date to datetime
    data['time'] = pd.to_datetime(data['time'], utc=True)
    # parse bits
    cols = [x for x in data.columns if x not in ['time', 'mdr', 'track']]
    for col in cols:
        match = re.search(r'C(\d{2})_(\d{2})', col)
        code_max, code_min = int(match.group(1)), int(match.group(2))
        for bit in range(8):
            code = code_max - bit
            if code >= code_min:
                new_col = f'C{code}'
                data[new_col] = data[col].str[bit].astype('int')
    # parse mdr and track
    data['track'] = data['track'].str.replace('T', '').astype('int')
    data['mdr'] = data['mdr'].str.replace('M', '').astype('int')
    # prepare for storage in influx
    data.dropna(axis=1, how='all', inplace=True)
    data.drop([x for x in data.columns if '_' in x], axis=1, inplace=True)
    data = data[['time'] + [x for x in list(data.columns) if x != 'time']]
    return data


def parse_records(lines: list):
    # remove unwanted lines
    lines = [x for x in lines if re.match(r'^\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', x)]
    lines = [x.split() for x in lines if len(x.split()) == 13]
    # data to dataframe
    data = pd.DataFrame(lines, dtype='str')
    data.columns = ['date', 'time', 'mdr', 'track',
                    'byte9', 'byte8', 'byte7', 'byte6', 'byte5',
                    'byte4', 'byte3', 'byte2', 'byte1']

    # parse track and mdr for json
    data['track'] = data['track'].str.replace('T', '')
    data['mdr'] = data['mdr'].str.replace('M', '')

    # format json field
    # {"byte1":"00000111","byte2":"00000011","byte3":"00000000","byte4":"00000101",
    #  "byte5":"00000000","byte6":"00000000","byte7":"00000000","byte8":"00000000",
    #  "byte9":"00000011","date":"01-02-18","mdr":"02","time":"07:04:51","track":"2"}
    data['json'] = data.to_dict(orient='records')
    data['json'] = [json.dumps(x) for x in data['json']]
    data['json'] = [b64encode(x.encode('utf-8')).decode('ascii') for x in data['json']]
    # date to datetime
    data['time'] = pd.to_datetime(data['date'] + 'T' + data['time'], utc=True)
    # convert mdr and track to integer
    data['track'] = data['track'].astype('int')
    data['mdr'] = data['mdr'].astype('int')
    # prepare for storage in influx
    data.dropna(axis=1, how='all', inplace=True)
    data.drop('date', axis=1, inplace=True)
    data = data[['time'] + [x for x in list(data.columns) if x != 'time']]
    # add dummy field
    data['dummy_field'] = '0'
    return data


def parse_ring_status(lines: list):
    ''' parse ring status logs '''
    try:
        data = parse_records(lines)
        data.sort_values('time', ascending=True, inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['count'] = data.groupby('time').cumcount().astype('int').astype('str')
        data.set_index('time', drop=True, inplace=True)
        if not data.empty:
            return data
        raise ServiceError('no records found')
    except (ServiceError, Exception) as err:
        raise ServiceError('no records found') from err
