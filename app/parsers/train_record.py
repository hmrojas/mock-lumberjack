'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
     Version: 0.0.x
    --------------------------------------------------
     Crossing API
     - train_data.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import re
import json
from base64 import b64encode

import pandas as pd

# common services
from ..services.common_services import ServiceError


def parse_records(lines: list):
    lines = [x for x in lines if re.match(r'^\d{2}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}', x)]
    lines = [x.split() for x in lines if 10 <= len(x.split()) <= 11]

    # data to dataframe
    data = pd.DataFrame(lines)
    if len(lines[0]) == 10:
        data.columns = ['date', 'time', 'track', 'mdrStatus', 'detSpeed', 'avgSpeed',
                        'islSpeed', 'detectMode', 'actualWarningTime',
                        'predictWarningTime']
    else:
        data.columns = ['date', 'time', 'track', 'mdrStatus', 'detSpeed', 'avgSpeed',
                        'islSpeed', 'direction', 'detectMode', 'actualWarningTime',
                        'predictWarningTime']

    # parse mdr and mdr status
    data['mdr'] = data['mdrStatus'].str.extract(r'(MDR\d{1,2})\(')
    data['mdrStatus'] = data['mdrStatus'].str.extract(r'\((\d{2})\)')
    # store each raw record as json
    # {"actualWarningTime":"30","avgSpeed":"54","date":"04-06-21","detSpeed":"50",
    # "detectMode":"CW","direction":"East R-T","islSpeed":"50","mdr":"MDR1","mdrStatus":"01",
    # "predictWarningTime":"30","time":"14:30:07","track":"1"}
    data['json'] = data.astype('str').to_dict(orient='records')
    data['json'] = [json.dumps(x) for x in data['json']]
    data['json'] = [b64encode(x.encode('utf-8')).decode('ascii') for x in data['json']]
    # date to datetime
    data['time'] = pd.to_datetime(data['date'] + 'T' + data['time'], utc=True)
    # parse numberic columns
    cols = ['track', 'detSpeed', 'avgSpeed', 'islSpeed', 'actualWarningTime',
            'predictWarningTime']
    for col in cols:
        data[col] = pd.to_numeric(data[col], errors='coerce')
    # prepare for storage in influx
    data.dropna(axis=1, how='all', inplace=True)
    data.drop('date', axis=1, inplace=True)
    data = data[['time'] + [x for x in list(data.columns) if x != 'time']]
    # add dummy field
    data['dummy_field'] = '0'
    return data


def parse_train_record(lines: list):
    try:
        data = parse_records(lines)
        data.sort_values('time', ascending=True, inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['count'] = data.groupby('time').cumcount().astype('int').astype('str')
        data.set_index('time', drop=True, inplace=True)
        if not data.empty:
            return data
        raise ServiceError('no train moves found')
    except (ServiceError, Exception) as err:
        raise ServiceError('no records found') from err
