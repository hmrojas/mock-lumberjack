'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
     Version: 0.0.x
    --------------------------------------------------
     Crossing API
     - recorder_data.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import re
import json
from base64 import b64encode

import pandas as pd

# common services
from ..services.common_services import ServiceError


def format_json(data):
    # {"header":{"date":"04-06-21","time":"12:03:18","type":"vital"},
    # "variables":[{"value":true,"variable":"T1RXC1"},{"value":true,"variable":"T2RXC1"}]}
    data['json'] = data[['date', 'time', 'type']].to_dict(orient='records')
    cols = [x for x in data.columns if x not in ['date', 'time', 'type', 'json']]
    records = data[cols].to_dict(orient='records')
    # group variables
    new_records = []
    for record in records:
        variables = []
        for key, value in record.items():
            if value == 1:
                variables.append(dict(value=True, variable=key))
            elif value == 0:
                variables.append(dict(value=False, variable=key))
        new_records.append(variables)
    # format json
    data['json'] = [{'header': x, 'variables': y} for x, y in zip(data['json'], new_records)]
    data['json'] = [json.dumps(x) for x in data['json']]
    data['json'] = [b64encode(x.encode('utf-8')).decode('ascii') for x in data['json']]
    return data['json']


def parse_records(lines: list):
    """read data recorder logs into a dataframe

    Args:
        lines (list): A list of log lines

    Returns:
        dataframe: dataframe with tidy data
    """
    # serialize timestamped records
    start = None
    records = []
    for line in lines:
        match = re.match(r'^(\d{2}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2}) (.+?)', line)
        if match and start is None:
            record = 'date=' + match.group(1) + ',' + 'time=' + match.group(2) + ','
            record += 'type=nonVital,' if 'Non-Vital' in match.group(3) else 'type=vital,'
            start = True
        elif not match and start:
            line = line.replace('=T', '=T,')
            line = line.replace('=F', '=F,')
            line = re.sub(r'\s+', '', line)
            record += line
        elif match and start:
            records.append(record[:-1])
            record = 'date=' + match.group(1) + ',' + 'time=' + match.group(2) + ','
            record += 'type=nonVital,' if 'Non-Vital' in match.group(3) else 'type=vital,'

    # convert into a list of dictionaries
    new_records = []
    for record in records:
        new_record = dict()
        for field in record.split(','):
            match = re.search(r'.+=', field)
            if match:
                value = re.sub(r'.+=', '', field)
                if value == 'T':
                    value = 1
                elif value == 'F':
                    value = 0
                new_record[match.group(0)[:-1]] = value
        new_records.append(new_record)

    # data to dataframe
    data = pd.DataFrame(new_records)

    # format json
    data['json'] = format_json(data)

    # date to datetime
    data['time'] = pd.to_datetime(data['date'] + 'T' + data['time'], utc=True)
    data.drop('date', axis=1, inplace=True)

    # convert to boolean
    cols = [x for x in list(data.columns) if x not in ['time', 'json', 'type']]
    for col in cols:
        data[col] = data[col].astype('boolean')
        data[col] = data[col].astype('boolean')

    # add dummy_field
    data['dummy_field'] = '0'
    return data


def parse_recorder_data(lines: list):
    ''' parse recorder data logs '''
    try:
        lines = [x for x in lines if len(x) > 1]
        data = parse_records(lines)
        data.sort_values('time', ascending=True, inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['count'] = data.groupby('time').cumcount().astype('int').astype('str')
        # forward fill na...
        # data = data.fillna(method='ffill')
        data.set_index('time', drop=True, inplace=True)
        if not data.empty:
            return data
        raise ServiceError('no train moves found')
    except (ServiceError, Exception) as err:
        raise ServiceError('no records found') from err
