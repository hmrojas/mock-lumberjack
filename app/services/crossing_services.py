'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Services for Crossing API
     - crossing_services.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import os
import json
import re
from base64 import b64decode
import datetime as dt
from typing import Optional

import pandas as pd
from fastapi import Query

from influxdb.line_protocol import quote_literal, quote_ident
from influxdb.resultset import ResultSet

# databases
from ..datalake.timeseries import query_influx

from ..datalake.insights import (
    activate_database,
    activate_collection,
    persist_asset_data
)

# common services
from .common_services import (
    LogType,
    ServiceError,
    NotFoundError,
    ValidationError
)


# Opens the train move database or creates it if non-existent
DATABASE = activate_database(database_name='train_moves')
COLLECTION = activate_collection(collection_name='moves', database=DATABASE)


# pylint: disable=R0201, R0903
# pylint: disable=R0902, R0913

class CrossingServices:
    def __init__(self,
                 lc_id: str,
                 device_id: str,
                 start_time: str = Query(..., alias='startTime'),
                 end_time: str = Query(..., alias='endTime'),
                 train_records: Optional[bool] = Query(True, alias='trainRecords'),
                 system_events: Optional[bool] = Query(True, alias='systemEvents'),
                 ring_status: Optional[bool] = Query(True, alias='ringStatus'),
                 recorder_data: Optional[bool] = Query(True, alias='recorderData'),
                 errors: Optional[bool] = Query(True, alias='errors'),
                 archive: Optional[bool] = False,
                 details: Optional[bool] = True):
        self.lc_id = lc_id
        self.device_id = device_id
        self.start_time = start_time
        self.end_time = end_time
        self.train_records = train_records
        self.system_events = system_events
        self.ring_status = ring_status
        self.recorder_data = recorder_data
        self.errors = errors
        self.archive = archive
        self.details = details
        self.database = f'{lc_id}_{device_id}_parsed'

    async def get_train_moves_details(self):  # pylint: disable=R0914, R0912
        try:
            moves = self.get_train_moves()
            if not moves:
                return []

            if not self.details:
                return moves

            # get details for all train moves
            moves_start = moves[0]['startTime']
            moves_end = moves[-1]['endTime']
            tags = dict()

            # get train move related data
            tags = {
                'trainRecords': self.__get_train_records(moves_start, moves_end),
                'systemEvents': self.__get_system_events(moves_start, moves_end),
                'recorderData': self.__get_recorder_events(moves_start, moves_end),
                'ringStatus': self.__get_ring_statuses(moves_start, moves_end),
                'errors': self.__get_errors(moves_start, moves_end)
            }

            # tag each train move with related data
            tagged_moves = []
            for move in moves:
                move_start = dt.datetime.fromisoformat(move['startTime'].replace('Z', '+00:00'))
                move_end = dt.datetime.fromisoformat(move['endTime'].replace('Z', '+00:00'))
                for key, data in tags.items():
                    move[key] = []
                    for item in data:
                        item_time = dt.datetime.fromisoformat(item['time'].replace('Z', '+00:00'))
                        if move_start <= item_time <= move_end:
                            move[key].append(item)
                    for item in move[key]:
                        tags[key].remove(item)
                    if not move[key]:
                        move.pop(key)
                tagged_moves.append(move)
            if self.archive:
                to_database(tagged_moves)
            return tagged_moves
        except ServiceError as err:
            raise ServiceError(str(err)) from err

    def validate_database_name(self):
        try:
            response = query_influx('SHOW DATABASES')
            if not response:
                raise NotFoundError(f'database "{self.database}" not found')
            for database in list(response.get_points()):
                if database['name'] == self.database:
                    return True
            raise NotFoundError(f'database "{self.database}" not found')
        except Exception as err:
            raise ValidationError(str(err)) from err

    def validate_time_range(self):
        try:
            if not datetime_validator(self.start_time):
                raise ValidationError('invalid datetime: startTime')
            if not datetime_validator(self.end_time):
                raise ValidationError('invalid datetime: endTime')
            if pd.to_datetime(self.start_time) >= pd.to_datetime(self.end_time):
                raise ValidationError('invalid datetime range: startTime >= endTime')
            return True
        except Exception as err:  # pylint: disable=broad-except
            raise ValidationError(f'invalid datetime range: {str(err)}') from err

    def get_train_moves(self):
        try:
            measurement = get_measurement(self.database, LogType.TRAIN_DATA)
            query = get_recording_starts(measurement, self.start_time, self.end_time)
            response = query_influx(query)
            if not response:
                return []

            # use "Recording Start" timestamps as start times and subsequent Recording Start
            # timestamps as end times
            start_times = [point['time'] for point in list(response.get_points())]
            end_times = start_times.copy()
            del end_times[0]
            end_times.append(self.end_time)

            query = ''
            for start_temp, end_temp in zip(start_times, end_times):
                query += get_oldest_time(measurement, start_temp, end_temp)
            response = query_influx(query)
            if not response:
                return []
            if isinstance(response, ResultSet):
                response = [response]

            # replace initial end times with actual data end time for
            # each train move
            end_times = [list(item.get_points())[0]['time'] for item in response]

            return [
                {'startTime': x, 'endTime': y, 'deviceId': self.device_id} for x, y
                in zip(start_times, end_times)
            ]
        except Exception as err:
            raise ServiceError(str(err)) from err

    def __get_system_events(self, start_time, end_time):  # pylint: disable=R0914
        try:
            if not self.system_events:
                return None

            measurement = get_measurement(self.database, LogType.SYSTEM_EVENT)
            fields = ['/mdr/', '/aux/', '/cwe/', '/ap/', '/isl/', '/Track/']
            query = get_records_by_time(measurement, fields, start_time, end_time)
            response = query_influx(query)
            if not response:
                return []

            # format response
            events = []
            for item in list(response.get_points()):
                event = dict(time=item['time'], mdr=[], aux=[], cwe=[], ap=[])
                item.pop('time')
                track_info = [dict(), dict(), dict(), dict()]
                for key, value in item.items():
                    var = re.sub(r'\d+', '', key)
                    num = key.replace(var, '')
                    if var in ['mdr', 'aux', 'cwe', 'ap']:
                        event[var].append({'statusNum': int(num), var: value})
                    else:
                        index = int(num) - 1
                        var = var.lower().replace('track', '')
                        track_info[index].update({'track': int(num), var: value})
                event['trackInfo'] = [x for x in track_info if any(x.values())]
                events.append({k: v for k, v in event.items() if v})
            return events
        except ServiceError:
            return []  # raise ServiceError(str(err)) from err

    def __get_system_events_explore(self, start_time, end_time):  # pylint: disable=R0914
        try:
            # from Crossing System Event Log get records within time range
            measurement = get_measurement(self.database, LogType.SYSTEM_EVENT)
            fields = ['/mdr/', '/aux/', '/cwe/', '/ap/', '/isl/', '/Track/']
            query = get_records_by_time(measurement, fields, start_time, end_time)
            response = query_influx(query)
            if not response:
                return []

            system_events = list(response.get_points())
            timestamps = [event['time'] for event in system_events]

            # from Train Data Logs get previous record
            measurement = get_measurement(self.database, LogType.TRAIN_DATA)
            query = ''
            fields += ['count']
            for timestamp in timestamps:
                query += get_previous_record(measurement, fields, timestamp)

            response = query_influx(query)
            if not response:
                return []
            if isinstance(response, ResultSet):
                response = [response]

            rows = []
            for item in response:
                for list_item in list(item.get_points()):
                    rows.append(list_item)

            # find status that triggered event
            data = pd.DataFrame(rows)
            data.sort_values(['time', 'count'], inplace=True)
            data.drop('count', axis=1, inplace=True)
            data.drop_duplicates(keep='last', inplace=True)
            data.set_index('time', inplace=True)
            data = data.diff()
            data.dropna(inplace=True)
            # data = data.loc[:, ~(data == 0).all()]
            data.reset_index(inplace=True)
            data.drop_duplicates(keep='last', inplace=True)
            data = data[data['time'].isin(timestamps)]
            print(data.head())
            changes = data.to_dict('records')

            # format response
            events = []
            for item, change in zip(system_events, changes):
                event = dict(time=item['time'], mdr=[], aux=[], cwe=[], ap=[])
                item.pop('time')
                change.pop('time')
                track_info = [dict(), dict(), dict(), dict()]
                for key, value in item.items():
                    var = re.sub(r'\d+', '', key)
                    num = key.replace(var, '')
                    if var in ['mdr', 'aux', 'cwe', 'ap'] and change[key]:
                        event[var].append({'statusNum': int(num), var: value})
                    elif (var == 'isl' and change[key]) or re.search(r'Track', var):
                        var = var.lower().replace('track', '')
                        track_info[int(num) - 1].update({'track': int(num), var: value})
                event['trackInfo'] = [x for x in track_info if any(x.values())]
                events.append({k: v for k, v in event.items() if v})
            return events
        except ServiceError:
            return []  # raise ServiceError(str(err)) from err

    def __get_errors(self, start_time, end_time):
        try:
            if not self.errors:
                return None

            measurement = get_measurement(self.database, LogType.ERROR)
            query = get_records_by_time(measurement, ['data'], start_time, end_time)
            response = query_influx(query)
            if not response:
                return []

            # format response
            errors = []
            for record in list(response.get_points()):
                error = dict(
                    time=record['time'],
                    message=b64decode(record['data'].encode('utf-8')).decode('ascii')
                )
                errors.append(error)
            return errors
        except ServiceError:
            return []  # raise ServiceError(str(err))

    def __get_train_records(self, start_time, end_time):
        try:
            if not self.train_records:
                return None

            measurement = get_measurement(self.database, LogType.TRAIN_RECORD)
            fields = ['/mdr/', '/Speed/', '/Mode/', '/Warning/', '/track/']
            query = get_records_by_time(measurement, fields, start_time, end_time)
            response = query_influx(query)
            if not response:
                return []
            return list(response.get_points())
        except ServiceError:
            return []  # raise ServiceError(str(err))

    def __get_recorder_events(self, start_time, end_time):
        try:
            if not self.recorder_data:
                return None

            # get one record
            measurement = get_measurement(self.database, LogType.RECORDER_DATA)
            query = f'SELECT * FROM {measurement} LIMIT 1'
            response = query_influx(query)
            if not response:
                return []

            # get fields
            fields = []
            for key in list(response.get_points())[0].keys():
                if key not in ['time', 'count', 'type', 'json', 'dummy_field']:
                    fields.append(key)

            # get data
            query = get_vital_events(measurement, fields, start_time, end_time)
            response = query_influx(query)
            if not response:
                return []

            # format response
            events = []
            for item in list(response.get_points()):
                time = item['time']
                item.pop('time')
                events.append(
                    {'time': time,
                     'signals': [{'key': k, 'value': v} for k, v in item.items() if v is not None]}
                )
            return events
        except ServiceError:
            return []  # raise ServiceError(str(err))

    def __get_ring_statuses(self, start_time, end_time):
        try:
            if not self.ring_status:
                return None

            measurement = get_measurement(self.database, LogType.RING_STATUS)
            fields = ['track', 'mdr', '/byte/']
            query = get_records_by_time(measurement, fields, start_time, end_time)
            response = query_influx(query)
            if not response:
                return []

            # format response
            ring_statuses = []
            for record in list(response.get_points()):
                status = dict(time=record['time'], mdr=None, track=None, codes=[])
                record.pop('time')
                for key, value in record.items():
                    if key.startswith('byte') and value != '00000000':
                        num = int(key.replace('byte', '')) - 1
                        status['codes'] += [
                            {'code': (8 - x) + 8*num} for x in range(7, -1, -1) if value[x] == '1'
                        ]
                    elif key in ['mdr', 'track']:
                        status[key] = value
                ring_statuses.append(status)
            return ring_statuses
        except ServiceError:
            return []  # raise ServiceError(str(err)) from err


def get_ring_codes():
    try:
        file_path = os.path.abspath(os.path.join(os.getcwd(), 'app/static/ring/codes.json'))
        with open(file_path, 'r') as json_file:
            ring = json.load(json_file)
        ring_codes = sorted(ring['ring_status_codes'], key=lambda x: x['code'])
        if len(ring_codes) != 72:
            return None
        for idx, item in enumerate(ring_codes):
            if item['code'] != idx + 1:
                return None
        return ring_codes
    except Exception:  # pylint: disable=broad-except
        return None


def get_measurement(database: str, log_type: LogType):
    database = quote_ident(database)
    policy = quote_ident(f'{log_type.value}_ret_pol')
    measurement = quote_ident(log_type.value)
    return '{0}.{1}.{2}'.format(database, policy, measurement)


def get_oldest_time(measurement: str, start_time: str, end_time: str):
    query = (
        'SELECT dummy_field FROM {0} WHERE time >= {1} AND time < {2} ORDER BY time DESC LIMIT 1;'
    )
    return query.format(measurement, quote_literal(start_time), quote_literal(end_time))


def get_recording_starts(measurement: str, start_time: str, end_time: str):
    query = 'SELECT recording FROM {0} WHERE time >= {1} AND time <= {2} ORDER BY time ASC;'
    return query.format(measurement, quote_literal(start_time), quote_literal(end_time))


def get_records_by_time(measurement: str, fields: list, start_time: str, end_time: str):
    query = 'SELECT {0} FROM {1} WHERE time >= {2} AND time <= {3} ORDER BY time ASC;'
    fields = ', '.join(x for x in fields)
    return query.format(fields, measurement, quote_literal(start_time), quote_literal(end_time))


def get_vital_events(measurement: str, fields: list, start_time: str, end_time: str):
    query = (
        'SELECT {0} FROM {1} WHERE type = {2} AND time >= {3} AND time <= {4} ORDER BY time ASC;'
    )
    fields = ', '.join(x for x in fields)
    query = query.format(
        fields, measurement, quote_literal('vital'),
        quote_literal(start_time), quote_literal(end_time)
    )
    return query


def get_previous_record(measurement: str, fields: list, timestamp: str) -> str:
    query = 'SELECT {0} FROM {1} WHERE time <= {2} ORDER BY time DESC LIMIT 8;'
    fields = ', '.join(f'{x}' for x in fields)
    return query.format(fields, measurement, quote_literal(timestamp))


def datetime_validator(date_time: str):
    try:
        valid = True
        dt.datetime.strptime(date_time, '%Y-%m-%d')
    except Exception:  # pylint: disable=broad-except
        try:
            dt.datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%SZ')
        except Exception:  # pylint: disable=broad-except
            try:
                dt.datetime.strptime(date_time, '%Y-%m-%dT%H:%M:%S.%fZ')
            except Exception:  # pylint: disable=broad-except
                valid = False
    return valid


def delete_all():
    # delete any previously store data
    for move in COLLECTION.fetchAll():
        if move:
            move.delete()


def to_database(moves: list):
    delete_all()

    # persist each train move data
    for item in moves:
        move_id = item['startTime'] + '-' + item['endTime']
        persist_asset_data(data=item, collection=COLLECTION, device_id=move_id)
