'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Log Capturing API
     - logcap.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''

from typing import Optional, List

from fastapi import Body
from pydantic import BaseModel  # pylint: disable=no-name-in-module

from .common_services import (
    get_mode,
    DuplicateError,
    NotFoundError,
    TestingModeError
)


# pylint: disable=R0903, R0201
class Description(BaseModel):
    ''' description '''
    description: str


class Settings(BaseModel):
    ''' settings '''
    name: str
    settings: Optional[Description]


class HealthStatus(BaseModel):
    health: str
    name: str
    state: str


class Lumberjack(BaseModel):
    lumberjack: List[HealthStatus] = []


class LogCapStatusModel(BaseModel):
    status: Optional[Lumberjack]
    softwareVersions: List[dict] = [{
        'component': 'lumberjack_restapi',
        'version': 'v2.4.0'
        }
    ]


LJ_STATUS = LogCapStatusModel(status=Lumberjack(lumberjack=[]))


class LogCapStatus:
    def status(self):
        if get_mode() != 'normal':
            raise TestingModeError(int(get_mode()), f'{get_mode()} Testing Mode')
        return LJ_STATUS.dict(exclude_unset=True)


class LogCapCreate:
    def __init__(
        self,
        new_instance: Settings = Body(..., alias='lumberjack')
    ):
        self.new_instance = new_instance

    def create(self):
        global LJ_STATUS  # pylint: disable=global-statement

        if get_mode() != 'normal':
            raise TestingModeError(int(get_mode()), f'{get_mode()} Testing Mode')

        name = self.new_instance.name
        instance = HealthStatus(health='HEALTHY', name=name, state='Running')
        for item in LJ_STATUS.status.lumberjack:
            if item.name == name:
                raise DuplicateError('Instance already exist')
        LJ_STATUS.status.lumberjack.append(instance)
        return {'result': f'{name} instance created'}


class LogCapDelete:
    def __init__(
        self,
        lc_id: str
    ):
        self.lc_id = lc_id

    def delete(self):
        global LJ_STATUS  # pylint: disable=global-statement

        if get_mode() != 'normal':
            raise TestingModeError(int(get_mode()), f'{get_mode()} Testing Mode')

        name = self.lc_id
        status = LJ_STATUS.status
        for item in status.lumberjack:
            if item.name == name:
                LJ_STATUS.status.lumberjack.remove(item)
                return {'result': f'{name} instance deleted'}
        raise NotFoundError('not found error')
