'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Services for Reader API
     - reader_services.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
import os
import re
from typing import Optional
from datetime import datetime as dt
from base64 import b64encode, b64decode

import pandas as pd
from fastapi import File
from influxdb.line_protocol import quote_ident

# parsers
from ..parsers.train_data import parse_train_data
from ..parsers.train_data import parse_system_event
from ..parsers.train_record import parse_train_record
from ..parsers.ring_status import parse_ring_status
from ..parsers.recorder_data import parse_recorder_data
from ..parsers.error import parse_error_logs

# databases
from ..datalake import timeseries as db

# common services
from .common_services import (
    LogType,
    ServiceError
)


# pylint: disable=R0903
class DataRetrieval:
    def __init__(    # pylint: disable=R0913
            self,
            lc_id: str,
            device_id: str,
            log_type: LogType,
            ascending: Optional[bool] = True,
            limit: Optional[int] = 1):
        self.log_type = log_type.value
        self.database = f'{lc_id}_{device_id}'
        self.ascending = ascending
        self.limit = limit

    def get_raw_data(self):
        try:
            database = quote_ident(f'{self.database}_archived')
            policy = quote_ident(f'{self.log_type}_ret_pol')
            measurement = quote_ident(self.log_type)
            database = '{0}.{1}.{2}'.format(database, policy, measurement)
            query = 'SELECT data FROM {0} ORDER by time {1} LIMIT {2}'
            query = query.format(database, 'ASC' if self.ascending else 'DESC', self.limit)
            response = db.query_influx(query)
            if not response:
                return 'no raw data'
            text = ''
            for item in list(response.get_points()):
                text += b64decode(item['data'].encode('utf-8')).decode('ascii')
            return text
        except ServiceError as err:
            raise ServiceError(str(err)) from err


class ParsePersist:
    def __init__(  # pylint: disable=R0913
            self,
            lc_id: str,
            device_id: str,
            log_type: LogType,
            data: bytes = File(...),
            replicate: Optional[int] = 1):
        self.log_type = log_type.value
        self.database = f'{lc_id}_{device_id}'
        self.data = data
        self.replicate = replicate
        self.parser = {
            'crossingTrainDataLog': parse_train_data,
            'crossingSystemEventLog': parse_system_event,
            'crossingTrainRecordLog': parse_train_record,
            'crossingRingStatusLog': parse_ring_status,
            'dataLog': parse_recorder_data,
            'errorLog': parse_error_logs
        }

    async def parse_and_persist(self):
        try:
            self.data = self.data.decode('ascii')
            parsed_data = self.parser[self.log_type](self.data.split('\r\n'))
            for day in range(self.replicate):
                if day != 0:
                    parsed_data.reset_index(inplace=True)
                    parsed_data['time'] -= pd.to_timedelta(1, unit='days')
                    parsed_data.set_index('time', drop=True, inplace=True)
                    data_time = dt.strftime(parsed_data.index[0], '%m-%d-%y')
                    self.data = re.sub(r'\d{2}-\d{2}-\d{2}', data_time, self.data)
                db.to_influx(  # parsed database
                    data=parsed_data,
                    database=f'{self.database}_parsed',
                    measurement=self.log_type,
                    tag_columns=['count'],
                    retention_policy_names=[x.value for x in LogType]
                )
                db.to_influx(  # archived database
                    data=to_b64(self.data),
                    database=f'{self.database}_archived',
                    measurement=self.log_type,
                    tag_columns=None,
                    retention_policy_names=[x.value for x in LogType]
                )
            return {'result': f'data persisted in {self.database}'}
        except ServiceError as err:
            raise ServiceError(str(err)) from err


class ReaderDelete:
    def __init__(
            self,
            lc_id: str,
            device_id: str,
            database_type: str):
        self.database = f'{lc_id}_{device_id}_{database_type}'

    def delete(self):
        try:
            return {'result': db.delete(self.database)}
        except ServiceError as err:
            raise ServiceError(str(err)) from err


def to_b64(data):
    # unit-testing
    # file_path = 'c:/users/295343/documents/ccn_sd_and_ai/mock-lumberjack/app'
    # file_path += '/static/data/2021-03-09 - MP326.81 - 10.145.30.131 - TD.log'
    # with open(file_path, "r") as log_file:
    #     data = log_file.read()
    data = pd.DataFrame(
        {
            'time': [dt.now()],
            'dummy_field': ['1'],
            'data': [b64encode(data.encode('utf-8')).decode('ascii')],
            'end_time': [dt.utcnow().strftime('%Y-%m-%d %H:%M:%S')],
            'read_type': ['ALL']
        }
    )
    data.set_index('time', drop=True, inplace=True)
    return data


def test_log():
    ''' test logic '''
    relative_path = 'app/static/data/2021-03-09 - MP326.81 - 10.145.30.131 - TD.log'
    file_path = os.path.abspath(os.path.join(os.getcwd(), relative_path))

    with open(file_path, "r") as log_file:
        lines = log_file.readlines()

    lines = [x.replace('\n', '') for x in lines]
    data = parse_train_data(lines)
    db.to_influx(
        data=data,
        database='uiMain1_parsed',
        measurement='crossingTrainDataLog',
        tag_columns=['count'],
        retention_policy_names=['crossingTrainDataLog'])
    db.is_present('uiMain1_parsed')
