from enum import Enum

from pydantic import BaseModel  # pylint: disable=E0611


class LogType(str, Enum):
    TRAIN_DATA = 'crossingTrainDataLog'
    SYSTEM_EVENT = 'crossingSystemEventLog'
    TRAIN_RECORD = 'crossingTrainRecordLog'
    RING_STATUS = 'crossingRingStatusLog'
    RECORDER_DATA = 'dataLog'
    ERROR = 'errorLog'


class Mode(str, Enum):
    ''' modes '''
    NORMAL = 'normal'
    RESPONSE_200 = '200'
    RESPONSE_500 = '500'
    RESPONSE_404 = '404'


class Result(BaseModel):  # pylint: disable=R0903
    result: str


class ServiceError(Exception):
    def __init__(self, message='Internal Server Error'):
        self.message = message
        super().__init__(message)


class DuplicateError(Exception):
    def __init__(self, message='Duplicate Error'):
        self.message = message
        super().__init__(message)


class NotFoundError(Exception):
    def __init__(self, message='Not Found Error'):
        self.message = message
        super().__init__(message)


class ValidationError(Exception):
    def __init__(self, message='Validation Error'):
        self.message = message
        super().__init__(message)


class TestingModeError(Exception):
    def __init__(self, status_code, message=None):
        self.status_code = status_code
        self.message = message
        super().__init__(status_code)


MODE = Mode('normal')


def get_mode() -> str:
    ''' return mode '''
    return MODE.value


def set_mode(app_mode: Mode):
    global MODE  # pylint: disable=global-statement
    MODE = app_mode
    return get_mode()
