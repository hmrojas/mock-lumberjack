'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Crossing API
     - crossing_models.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
from typing import Optional, List
from enum import Enum

from pydantic import BaseModel, Field  # pylint: disable=E0611


class MDR(str, Enum):
    MDR1 = 'MDR1'
    MDR2 = 'MDR2'
    MDR3 = 'MDR3'
    MDR4 = 'MDR4'
    MDR5 = 'MDR5'
    MDR6 = 'MDR6'
    MDR7 = 'MDR7'
    MDR8 = 'MDR8'
    MDR9 = 'MDR9'
    MDR10 = 'MDR10'
    MDR11 = 'MDR11'
    MDR12 = 'MDR12'


class DetectMode(str, Enum):
    AUX = 'AUX'
    CW = 'CW'
    MD = 'MD'


# pylint: disable=C0103, R0903

class RingCodes(BaseModel):
    code: int


class RingStatus(BaseModel):
    time: str
    mdr: int
    track: int
    codes: Optional[List[RingCodes]]


class MdrStatuses(BaseModel):
    statusNum: Optional[int]
    mdr: Optional[bool]


class AuxStatuses(BaseModel):
    statusNum: Optional[int]
    aux: Optional[bool]


class CweStatuses(BaseModel):
    statusNum: Optional[int]
    cwe: Optional[bool]


class ApStatuses(BaseModel):
    statusNum: Optional[int]
    ap: Optional[bool]


class KeyValue(BaseModel):
    key: str
    value: bool


class Errors(BaseModel):
    time: str
    message: str


class TrackInfo(BaseModel):
    track: Optional[int]
    isl: Optional[bool]
    ph: Optional[int]
    rx: Optional[int]


class SystemEvents(BaseModel):
    time: str
    trackInfo: Optional[List[TrackInfo]]
    mdr: Optional[List[MdrStatuses]]
    aux: Optional[List[AuxStatuses]]
    cwe: Optional[List[CweStatuses]]
    ap: Optional[List[ApStatuses]]


class RecorderData(BaseModel):
    time: str
    signals: List[KeyValue]


class TrainRecords(BaseModel):
    time: str
    mdr: MDR
    mdrStatus: str
    track: int
    avgSpeed: Optional[int]
    detSpeed: Optional[int]
    islSpeed: Optional[int]
    direction: Optional[str]
    detectMode: Optional[DetectMode]
    actualWarningTime: Optional[int]
    predictWarningTime: Optional[int]


class CrossingResponse(BaseModel):
    startTime: str
    endTime: str
    deviceId: str
    trainRecords: Optional[List[TrainRecords]] = None
    systemEvents: Optional[List[SystemEvents]] = None
    recorderData: Optional[List[RecorderData]] = None
    ringStatus: Optional[List[RingStatus]] = None
    errors: Optional[List[Errors]] = None

# pylint: enable=C0103, R0903
# print(CrossingResponse.schema_json())
