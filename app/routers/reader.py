'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Reader API
     - reader.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse, PlainTextResponse

# common services
from ..services.common_services import (
    Result,
    ServiceError,
)

from ..services.reader_services import (
    DataRetrieval,
    ParsePersist,
    ReaderDelete
)


reader = APIRouter()


@reader.get('/status')
async def reader_status():
    return {'result': 'reader API alive'}


@reader.get(
    '/logs/{lc_id}/{device_id}/{log_type}',
    responses={
        500: {'model': Result, 'description': 'Reader Internal Error'}}
)
async def get_raw_log(service: DataRetrieval = Depends(DataRetrieval)):
    try:
        return PlainTextResponse(status_code=200, content=service.get_raw_data())
    except ServiceError as err:
        return JSONResponse(status_code=500, content={'result': str(err)})


@reader.post(
    '/logs/{lc_id}/{device_id}/{log_type}',
    responses={
        500: {'model': Result, 'description': 'Reader Internal Error'}}
)
async def read_train_data_file(service: ParsePersist = Depends(ParsePersist)):
    try:
        return await service.parse_and_persist()
    except ServiceError as err:
        return JSONResponse(status_code=500, content={'result': str(err)})


@reader.delete(
    '/logs/{lc_id}/{device_id}',
    responses={
        500: {'model': Result, 'description': 'Reader Internal Error'}}
)
async def delete_database(service: ReaderDelete = Depends(ReaderDelete)):
    try:
        return service.delete()
    except ServiceError as err:
        return JSONResponse(status_code=500, content={'result': str(err)})
