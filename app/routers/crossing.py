'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Crossing API
     - crossing.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
from typing import List

from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse, UJSONResponse

# crossing services
from ..services.crossing_services import CrossingServices
from ..models.crossing_models import CrossingResponse


# common services
from ..services.common_services import (
    Result,
    ServiceError,
    NotFoundError,
    ValidationError
)


# Initialize crossing api (as API Router instance of main API)
crossing = APIRouter()


@crossing.get(
    '/{lc_id}/{device_id}/trainmoves',
    response_model=List[CrossingResponse],
    responses={
        404: {'description': 'Invalid Path or Query Parameter', 'model': Result},
        500: {'model': Result}}
)
async def read_items(services: CrossingServices = Depends(CrossingServices)):
    try:
        services.validate_database_name()
        services.validate_time_range()
        return UJSONResponse(status_code=200, content=await services.get_train_moves_details())
    except (NotFoundError, ValidationError) as err:
        return JSONResponse(status_code=404, content={'result': str(err)})
    except ServiceError as err:
        return JSONResponse(status_code=500, content={'result': str(err)})
