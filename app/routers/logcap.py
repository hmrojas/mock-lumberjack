'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Log Capturing API
     - logcap.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse


# logcap services
from ..services.logcap_services import (
    LogCapStatusModel,
    LogCapStatus,
    LogCapCreate,
    LogCapDelete
)

# common_services
from ..services.common_services import (
    Result,
    NotFoundError,
    DuplicateError,
    TestingModeError
)


logcap = APIRouter()


@logcap.get(
    '/status',
    response_model=LogCapStatusModel,
    responses={
        404: {'model': Result, 'description': 'Invalid Path or Query Parameter'},
        500: {'model': Result, 'description': 'LogCap Internal Error'}}
)
async def logcap_status(service: LogCapStatus = Depends(LogCapStatus)):
    try:
        return service.status()
    except TestingModeError as err:
        return JSONResponse(status_code=err.status_code, content={'result': str(err.message)})


@logcap.post(
    '/settings',
    response_model=Result,
    responses={
        404: {'model': Result, 'description': 'Invalid Path or Query Parameter'},
        500: {'model': Result, 'description': 'LogCap Internal Error'}}
)
async def logcap_create_instance(service: LogCapCreate = Depends(LogCapCreate)):
    try:
        return service.create()
    except DuplicateError as err:
        return JSONResponse(status_code=404, content={'result': str(err)})
    except TestingModeError as err:
        return JSONResponse(status_code=err.status_code, content={'result': str(err.message)})


@logcap.delete(
    '/settings/{lc_id}',
    response_model=Result,
    responses={
        404: {'model': Result, 'description': 'Invalid Path or Query Parameter'},
        500: {'model': Result, 'description': 'LogCap Internal Error'}}
)
async def logcap_delete_instance(service: LogCapDelete = Depends(LogCapDelete)):
    try:
        return service.delete()
    except NotFoundError as err:
        return JSONResponse(status_code=404, content={'result': str(err)})
    except TestingModeError as err:
        return JSONResponse(status_code=err.status_code, content={'result': str(err.message)})
