'''
    ==================================================
     ALSTOM - mobility by nature
    --------------------------------------------------
     Data API
    --------------------------------------------------
     Mode API
     - mode.py
    --------------------------------------------------
     NAM Innovation
     Data Science and AI
     Melbourne, FL
    --------------------------------------------------
     Copyright (c) 2021 - Alstom USA.
    ==================================================
'''
from fastapi import APIRouter, Depends

from ..services.common_services import (
    Result,
    get_mode,
    set_mode
)


mode = APIRouter()


@mode.get('/status', response_model=Result)
async def mode_status(current_mode: str = Depends(get_mode)):
    return {'result': f'Mode {current_mode}'}


@mode.put('/{app_mode}', response_model=Result)
def change_mode(current_mode: str = Depends(set_mode)):
    return {'result': f'Mode {current_mode}'}
