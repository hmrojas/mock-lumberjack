'''  main module '''

from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles
from fastapi.openapi.utils import get_openapi
from fastapi.openapi.docs import (get_swagger_ui_html,
                                  get_swagger_ui_oauth2_redirect_html)

from .routers import logcap
from .routers import reader
from .routers import mode
from .routers import crossing


app = FastAPI()


app.mount('/static', StaticFiles(directory='app/static'), name='static')


@app.get("/", tags=['General Info'])
def read_root():
    ''' root '''
    return {"Hello": "Mocking Lumberjack Server"}


app.include_router(logcap.logcap, prefix='/v2', tags=['Lumberjack'])
app.include_router(reader.reader, prefix='/v2/reader', tags=['Tools - Readers'])
app.include_router(mode.mode, prefix='/v2/mode', tags=['Tools - Modes'])
app.include_router(crossing.crossing, prefix='/v2/crossing', tags=['Tools - Crossing'])


# =======================
#  OpenAPI customization
# =======================

def custom_openapi():
    ''' Cache the openapi schema for subsequent requests. '''
    if app.openapi_schema:
        return app.openapi_schema

    # If it's the first request, customize the openapi schema.
    openapi_schema = get_openapi(title='Lumberjack Mock',
                                 version='Test Version',
                                 description='Lumberjack Simulation',
                                 routes=app.routes)
    openapi_schema['info']['x-logo'] = {'url': '/static/images/Alstom_Innovation.png'}
    app.openapi_schema = openapi_schema
    return app.openapi_schema


# Updates OpenAPI schema (overrides the default method)
app.openapi = custom_openapi


# Customize OpenAPI docs
@app.get('/docs', include_in_schema=False)
async def dataapi_docs():
    ''' OpenAPI documentation '''
    return get_swagger_ui_html(
                            openapi_url='/dataapi_openapi.json',
                            title='Mock Lumberjack Server',
                            oauth2_redirect_url='/docs/oauth2-redirect',
                            swagger_js_url='/static/js/swagger-ui-bundle.js',
                            swagger_css_url='/static/css/swagger-ui.css',
                            swagger_favicon_url='/static/images/favicon.ico')


@app.get(app.swagger_ui_oauth2_redirect_url, include_in_schema=False)
async def dataapi_docs_oauth2_redirect():
    ''' OpenAPI redirect endpoint '''
    return get_swagger_ui_oauth2_redirect_html()
