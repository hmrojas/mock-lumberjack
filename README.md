# README #


### What is this repository for? ###

* Simulates lumberjack_restapi application
* Parses, stores and archives log (text) files into InfluxDB (see list of supported log files.)
* Serves as a `sandbox` for data-api feature development
* Simulates status responses, including 404 and 500 type responses
* Supported logs:
  * Crossing Train Data Logs
  * Crossing Ring Status Logs
  * Crossing System Events Logs
  * Crossing Train Records Logs
  * Recorder Data Logs
  * Error Logs

### How do I get set up? ###

* App expects that production lumberjack_restapi application is not running and port number 8080 is available.

* Build docker image

``` docker
docker build -t 'mock-lumberjack' path/to/mock-lumberjack
```

* Run docker image

``` docker
docker run -p 8080:8080 --name lumberjack_restapi --network wsdmm-net -d mock-lumberjack;
```

* No configuration file is required.

* Navigate to [http://localhost:8080/docs] for OpenAPI documentation of REST API.
